
var app = angular.module('app', ['ngResource','ui.bootstrap']);

app.factory('notificationFactory', function () {
	return {
		success: function () {
			toastr.success("Success");
		},
		error: function (text) {
			toastr.error(text, "Error");
		}
	};
});

/*
 * Records
 */

app.factory('recordFactory', function ($resource) {
    return $resource('./api/admin/record/:id',
        { id: '@id',},
        {	update: { method: 'PUT' } ,
			query: { method: 'GET', isArray: false, params: {count: 10, page: 1}} 
		});
});

app.controller('RecordsCtrl', function ($scope, recordFactory, notificationFactory) {
    $scope.records = [];
	$scope.addMode = false;
	
	$scope.pageRecords = 1;
	$scope.count = 10;  
	$scope.total = 0;  
	
	$scope.toggleAddMode = function () {
		$scope.addMode = !$scope.addMode;
	};

	$scope.toggleEditMode = function (record) {
		record.editMode = !record.editMode;
	};
     
    var successCallback = function (e, cb) {
		notificationFactory.success();
		$scope.getRecords(cb);
	};

	var successPostCallback = function (e) {
		successCallback(e, function () {
			$scope.toggleAddMode();
			$scope.record = {};
		});
	};
 
	var errorCallback = function (e) {
		notificationFactory.error(e.data.Message);
	};
     
    $scope.addRecord = function () {
        recordFactory.save($scope.record, successPostCallback, errorCallback);
    };

	$scope.deleteRecord = function (record) {
		recordFactory.delete({ id: record.id }, successCallback, errorCallback);
	};

	$scope.updateRecord = function (record) {
		recordFactory.update({ id: record.id }, record, successCallback, errorCallback);
	};

	$scope.getRecords = function (cb) {
		recordFactory.get({count: $scope.count, page: $scope.pageRecords}, function (data) {
			$scope.records = data.result;
			$scope.total = data.total;
			if (cb) cb();
		});
	};

	$scope.getRecords();
	
	$scope.$watch("pageRecords", function() {
		$scope.getRecords();
	});
});


/*
 * Parcels
 */

app.factory('parcelFactory', function ($resource) {
    return $resource('./api/admin/parcel/:id',
        { id: '@id',},
        {	update: { method: 'PUT' } ,
			query: { method: 'GET', isArray: false, params: {count: 10, page: 1}} 
		});
});

app.controller('ParcelsCtrl', function ($scope, parcelFactory, notificationFactory) {
    $scope.records = [];
	$scope.addMode = false;
	
	$scope.pageParcels = 1;
	$scope.count = 10;  
	$scope.total = 0;  
	
	$scope.toggleAddMode = function () {
		$scope.addMode = !$scope.addMode;
	};

	$scope.toggleEditMode = function (record) {
		record.editMode = !record.editMode;
	};
     
    var successCallback = function (e, cb) {
		notificationFactory.success();
		$scope.getRecords(cb);
	};

	var successPostCallback = function (e) {
		successCallback(e, function () {
			$scope.toggleAddMode();
			$scope.record = {};
		});
	};
 
	var errorCallback = function (e) {
		notificationFactory.error(e.data.Message);
	};
     
    $scope.addRecord = function () {
        parcelFactory.save($scope.record, successPostCallback, errorCallback);
    };

	$scope.deleteRecord = function (record) {
		parcelFactory.delete({ id: record.id }, successCallback, errorCallback);
	};

	$scope.updateRecord = function (record) {
		parcelFactory.update({ id: record.id }, record, successCallback, errorCallback);
	};

	$scope.getRecords = function (cb) {
		parcelFactory.get({count: $scope.count, page: $scope.pageParcels}, function (data) {
			$scope.records = data.result;
			$scope.total = data.total;
			if (cb) cb();
		});
	};

	$scope.getRecords();
	
	$scope.$watch("pageParcels", function() {
		$scope.getRecords();
	});
});


/*
 * Regions
 */

app.factory('regionFactory', function ($resource) {
    return $resource('./api/admin/region/:id',
        { id: '@id',},
        {	update: { method: 'PUT' } ,
			query: { method: 'GET', isArray: false, params: {count: 10, page: 1}} 
		});
});

app.controller('RegionsCtrl', function ($scope, regionFactory, notificationFactory) {
    $scope.records = [];
	$scope.addMode = false;
	
	$scope.pageRegions = 1;
	$scope.count = 10;  
	$scope.total = 0;  
	
	$scope.toggleAddMode = function () {
		$scope.addMode = !$scope.addMode;
	};

	$scope.toggleEditMode = function (record) {
		record.editMode = !record.editMode;
	};
     
    var successCallback = function (e, cb) {
		notificationFactory.success();
		$scope.getRecords(cb);
	};

	var successPostCallback = function (e) {
		successCallback(e, function () {
			$scope.toggleAddMode();
			$scope.record = {};
		});
	};
 
	var errorCallback = function (e) {
		notificationFactory.error(e.data.Message);
	};
     
    $scope.addRecord = function () {
        regionFactory.save($scope.record, successPostCallback, errorCallback);
    };

	$scope.deleteRecord = function (record) {
		regionFactory.delete({ id: record.code }, successCallback, errorCallback);
	};

	$scope.updateRecord = function (record) {
		regionFactory.update({ id: record.code }, record, successCallback, errorCallback);
	};

	$scope.getRecords = function (cb) {
		regionFactory.get({count: $scope.count, page: $scope.pageRegions}, function (data) {
			$scope.records = data.result;
			$scope.total = data.total;
			if (cb) cb();
		});
	};

	$scope.getRecords();
	
	$scope.$watch("pageRegions", function() {
		$scope.getRecords();
	});
});


/*
 * Subregions
 */

app.factory('subregionFactory', function ($resource) {
    return $resource('./api/admin/subregion/:id',
        { id: '@id',},
        {	update: { method: 'PUT' } ,
			query: { method: 'GET', isArray: false, params: {count: 10, page: 1}} 
		});
});

app.controller('SubregionsCtrl', function ($scope, subregionFactory, notificationFactory) {
    $scope.records = [];
	$scope.addMode = false;
	
	$scope.pageSubregions = 1;
	$scope.count = 10;  
	$scope.total = 0;  
	
	$scope.toggleAddMode = function () {
		$scope.addMode = !$scope.addMode;
	};

	$scope.toggleEditMode = function (record) {
		record.editMode = !record.editMode;
	};
     
    var successCallback = function (e, cb) {
		notificationFactory.success();
		$scope.getRecords(cb);
	};

	var successPostCallback = function (e) {
		successCallback(e, function () {
			$scope.toggleAddMode();
			$scope.record = {};
		});
	};
 
	var errorCallback = function (e) {
		notificationFactory.error(e.data.Message);
	};
     
    $scope.addRecord = function () {
        subregionFactory.save($scope.record, successPostCallback, errorCallback);
    };

	$scope.deleteRecord = function (record) {
		subregionFactory.delete({ id: record.code }, successCallback, errorCallback);
	};

	$scope.updateRecord = function (record) {
		subregionFactory.update({ id: record.code }, record, successCallback, errorCallback);
	};

	$scope.getRecords = function (cb) {
		subregionFactory.get({count: $scope.count, page: $scope.pageSubregions}, function (data) {
			$scope.records = data.result;
			$scope.total = data.total;
			if (cb) cb();
		});
	};

	$scope.getRecords();
	
	$scope.$watch("pageSubregions", function() {
		$scope.getRecords();
	});
});


/*
 * Municipality
 */

app.factory('municipalityFactory', function ($resource) {
    return $resource('./api/admin/municipality/:id',
        { id: '@id',},
        {	update: { method: 'PUT' } ,
			query: { method: 'GET', isArray: false, params: {count: 10, page: 1}} 
		});
});

app.controller('MunicipalitiesCtrl', function ($scope, municipalityFactory, notificationFactory) {
    $scope.records = [];
	$scope.addMode = false;
	
	$scope.pageMunicipality = 1;
	$scope.count = 10;  
	$scope.total = 0;  
	
	$scope.toggleAddMode = function () {
		$scope.addMode = !$scope.addMode;
	};

	$scope.toggleEditMode = function (record) {
		record.editMode = !record.editMode;
	};
     
    var successCallback = function (e, cb) {
		notificationFactory.success();
		$scope.getRecords(cb);
	};

	var successPostCallback = function (e) {
		successCallback(e, function () {
			$scope.toggleAddMode();
			$scope.record = {};
		});
	};
 
	var errorCallback = function (e) {
		notificationFactory.error(e.data.Message);
	};
     
    $scope.addRecord = function () {
        municipalityFactory.save($scope.record, successPostCallback, errorCallback);
    };

	$scope.deleteRecord = function (record) {
		municipalityFactory.delete({ id: record.code }, successCallback, errorCallback);
	};

	$scope.updateRecord = function (record) {
		municipalityFactory.update({ id: record.code }, record, successCallback, errorCallback);
	};

	$scope.getRecords = function (cb) {
		municipalityFactory.get({count: $scope.count, page: $scope.pageMunicipality}, function (data) {
			$scope.records = data.result;
			$scope.total = data.total;
			if (cb) cb();
		});
	};

	$scope.getRecords();
	
	$scope.$watch("pageMunicipality", function() {
		$scope.getRecords();
	});
});


/*
 * Municipality
 */

app.factory('cadastralAreaFactory', function ($resource) {
    return $resource('./api/admin/cadastral-area/:id',
        { id: '@id',},
        {	update: { method: 'PUT' } ,
			query: { method: 'GET', isArray: false, params: {count: 10, page: 1}} 
		});
});

app.controller('CadastralAreaCtrl', function ($scope, cadastralAreaFactory, notificationFactory) {
    $scope.records = [];
	$scope.addMode = false;
	
	$scope.pageCadastralArea = 1;
	$scope.count = 10;  
	$scope.total = 0;  
	
	$scope.toggleAddMode = function () {
		$scope.addMode = !$scope.addMode;
	};

	$scope.toggleEditMode = function (record) {
		record.editMode = !record.editMode;
	};
     
    var successCallback = function (e, cb) {
		notificationFactory.success();
		$scope.getRecords(cb);
	};

	var successPostCallback = function (e) {
		successCallback(e, function () {
			$scope.toggleAddMode();
			$scope.record = {};
		});
	};
 
	var errorCallback = function (e) {
		notificationFactory.error(e.data.Message);
	};
     
    $scope.addRecord = function () {
        cadastralAreaFactory.save($scope.record, successPostCallback, errorCallback);
    };

	$scope.deleteRecord = function (record) {
		cadastralAreaFactory.delete({ id: record.code }, successCallback, errorCallback);
	};

	$scope.updateRecord = function (record) {
		cadastralAreaFactory.update({ id: record.code }, record, successCallback, errorCallback);
	};

	$scope.getRecords = function (cb) {
		cadastralAreaFactory.get({count: $scope.count, page: $scope.pageCadastralArea}, function (data) {
			$scope.records = data.result;
			$scope.total = data.total;
			if (cb) cb();
		});
	};

	$scope.getRecords();
	
	$scope.$watch("pageCadastralArea", function() {
		$scope.getRecords();
	});
});



/*
 * Building
 */

app.factory('buildingFactory', function ($resource) {
    return $resource('./api/admin/building/:id',
        { id: '@id',},
        {	update: { method: 'PUT' } ,
			query: { method: 'GET', isArray: false, params: {count: 10, page: 1}} 
		});
});

app.controller('BuildingCtrl', function ($scope, buildingFactory, notificationFactory) {
    $scope.records = [];
	$scope.addMode = false;
	
	$scope.pageBuilding = 1;
	$scope.count = 10;  
	$scope.total = 0;  
	
	$scope.toggleAddMode = function () {
		$scope.addMode = !$scope.addMode;
	};

	$scope.toggleEditMode = function (record) {
		record.editMode = !record.editMode;
	};
     
    var successCallback = function (e, cb) {
		notificationFactory.success();
		$scope.getRecords(cb);
	};

	var successPostCallback = function (e) {
		successCallback(e, function () {
			$scope.toggleAddMode();
			$scope.record = {};
		});
	};
 
	var errorCallback = function (e) {
		notificationFactory.error(e.data.Message);
	};
     
    $scope.addRecord = function () {
        buildingFactory.save($scope.record, successPostCallback, errorCallback);
    };

	$scope.deleteRecord = function (record) {
		buildingFactory.delete({ id: record.parcel_id}, successCallback, errorCallback);
	};

	$scope.updateRecord = function (record) {
		buildingFactory.update({ id: record.parcel_id}, record, successCallback, errorCallback);
	};

	$scope.getRecords = function (cb) {
		buildingFactory.get({count: $scope.count, page: $scope.pageBuilding}, function (data) {
			$scope.records = data.result;
			$scope.total = data.total;
			if (cb) cb();
		});
	};

	$scope.getRecords();
	
	$scope.$watch("pageBuilding", function() {
		$scope.getRecords();
	});
});



/*
 * AuthorizedPerson
 */

app.factory('authorizedPersonFactory', function ($resource) {
    return $resource('./api/admin/authorized-person/:id',
        { id: '@id',},
        {	update: { method: 'PUT' } ,
			query: { method: 'GET', isArray: false, params: {count: 10, page: 1}} 
		});
});

app.controller('AuthorizedPersonCtrl', function ($scope, authorizedPersonFactory, notificationFactory) {
    $scope.records = [];
	$scope.addMode = false;
	
	$scope.pageAuthorizedPerson = 1;
	$scope.count = 10;  
	$scope.total = 0;  
	
	$scope.toggleAddMode = function () {
		$scope.addMode = !$scope.addMode;
	};

	$scope.toggleEditMode = function (record) {
		record.editMode = !record.editMode;
	};
     
    var successCallback = function (e, cb) {
		notificationFactory.success();
		$scope.getRecords(cb);
	};

	var successPostCallback = function (e) {
		successCallback(e, function () {
			$scope.toggleAddMode();
			$scope.record = {};
		});
	};
 
	var errorCallback = function (e) {
		notificationFactory.error(e.data.Message);
	};
     
    $scope.addRecord = function () {
        authorizedPersonFactory.save($scope.record, successPostCallback, errorCallback);
    };

	$scope.deleteRecord = function (record) {
		authorizedPersonFactory.delete({ id: record.id}, successCallback, errorCallback);
	};

	$scope.updateRecord = function (record) {
		authorizedPersonFactory.update({ id: record.id}, record, successCallback, errorCallback);
	};

	$scope.getRecords = function (cb) {
		authorizedPersonFactory.get({count: $scope.count, page: $scope.pageAuthorizedPerson}, function (data) {
			$scope.records = data.result;
			$scope.total = data.total;
			if (cb) cb();
		});
	};

	$scope.getRecords();
	
	$scope.$watch("pageAuthorizedPerson", function() {
		$scope.getRecords();
	});
});


/*
 * NaturalPerson
 */

app.factory('naturalPersonFactory', function ($resource) {
    return $resource('./api/admin/natural-person/:id',
        { id: '@id',},
        {	update: { method: 'PUT' } ,
			query: { method: 'GET', isArray: false, params: {count: 10, page: 1}} 
		});
});

app.controller('NaturalPersonCtrl', function ($scope, naturalPersonFactory, notificationFactory) {
    $scope.records = [];
	$scope.addMode = false;
	
	$scope.pageNaturalPerson = 1;
	$scope.count = 10;  
	$scope.total = 0;  
	
	$scope.toggleAddMode = function () {
		$scope.addMode = !$scope.addMode;
	};

	$scope.toggleEditMode = function (record) {
		record.editMode = !record.editMode;
	};
     
    var successCallback = function (e, cb) {
		notificationFactory.success();
		$scope.getRecords(cb);
	};

	var successPostCallback = function (e) {
		successCallback(e, function () {
			$scope.toggleAddMode();
			$scope.record = {};
		});
	};
 
	var errorCallback = function (e) {
		notificationFactory.error(e.data.Message);
	};
     
    $scope.addRecord = function () {
        naturalPersonFactory.save($scope.record, successPostCallback, errorCallback);
    };

	$scope.deleteRecord = function (record) {
		naturalPersonFactory.delete({ id: record.authorized_person_id}, successCallback, errorCallback);
	};

	$scope.updateRecord = function (record) {
		naturalPersonFactory.update({ id: record.authorized_person_id}, record, successCallback, errorCallback);
	};

	$scope.getRecords = function (cb) {
		naturalPersonFactory.get({count: $scope.count, page: $scope.pageNaturalPerson}, function (data) {
			$scope.records = data.result;
			$scope.total = data.total;
			if (cb) cb();
		});
	};

	$scope.getRecords();
	
	$scope.$watch("pageNaturalPerson", function() {
		$scope.getRecords();
	});
});


/*
 * AuthorizedPerson
 */

app.factory('legalPersonFactory', function ($resource) {
    return $resource('./api/admin/legal-person/:id',
        { id: '@id',},
        {	update: { method: 'PUT' } ,
			query: { method: 'GET', isArray: false, params: {count: 10, page: 1}} 
		});
});

app.controller('LegalPersonCtrl', function ($scope, legalPersonFactory, notificationFactory) {
    $scope.records = [];
	$scope.addMode = false;
	
	$scope.pageLegalPerson = 1;
	$scope.count = 10;  
	$scope.total = 0;  
	
	$scope.toggleAddMode = function () {
		$scope.addMode = !$scope.addMode;
	};

	$scope.toggleEditMode = function (record) {
		record.editMode = !record.editMode;
	};
     
    var successCallback = function (e, cb) {
		notificationFactory.success();
		$scope.getRecords(cb);
	};

	var successPostCallback = function (e) {
		successCallback(e, function () {
			$scope.toggleAddMode();
			$scope.record = {};
		});
	};
 
	var errorCallback = function (e) {
		notificationFactory.error(e.data.Message);
	};
     
    $scope.addRecord = function () {
        legalPersonFactory.save($scope.record, successPostCallback, errorCallback);
    };

	$scope.deleteRecord = function (record) {
		legalPersonFactory.delete({ id: record.authorized_person_id}, successCallback, errorCallback);
	};

	$scope.updateRecord = function (record) {
		legalPersonFactory.update({ id: record.authorized_person_id}, record, successCallback, errorCallback);
	};

	$scope.getRecords = function (cb) {
		legalPersonFactory.get({count: $scope.count, page: $scope.pageLegalPerson}, function (data) {
			$scope.records = data.result;
			$scope.total = data.total;
			if (cb) cb();
		});
	};

	$scope.getRecords();
	
	$scope.$watch("pageLegalPerson", function() {
		$scope.getRecords();
	});
});