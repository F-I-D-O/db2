
var app = angular.module('app', ["ngTouch", "angucomplete", 'ngTable', 'ngResource']);

app.value('statistics', { records: 0, parcels: 0, area: 0 });


app.controller('statisticController', function ($scope, statistics){
    $scope.statistics = statistics;
    $scope.statistics = statistics;
}); 

app.controller('FilterCtrl', function ($scope) {
	$scope.regions = JSON.parse(document.getElementById('filter-regions').getAttribute("data-regions"));
	
	// 0 means all regions - default
	$scope.selectedRegion = $scope.regions[0];
	
	$scope.subregionFilterParams = {};
	$scope.subregionFilterParams.region = function(){
		return $scope.selectedRegion.code;
	};
	
	$scope.selectedSubregion = 0;
	
	$scope.municipalityFilterParams = {};
	$scope.municipalityFilterParams.region = function(){
		return $scope.selectedRegion.code;
	};
	$scope.municipalityFilterParams.subregion = function(){
		return typeof $scope.selectedSubregion === 'object' ? 
			$scope.selectedSubregion.originalObject.code : $scope.selectedSubregion;
	};
});


app.directive('loadingContainer', function () {
	return {
		restrict: 'A',
		scope: false,
		link: function (scope, element, attrs) {
			var loadingLayer = angular.element('<div class="loading"></div>');
			element.append(loadingLayer);
			element.addClass('loading-container');
			scope.$watch(attrs.loadingContainer, function (value) {
				loadingLayer.toggleClass('ng-hide', !value);
			});
		}
	};
})

app.controller('ResultsCtrl', function ($scope, $timeout, $resource, NgTableParams, statistics) {
	var Api = $resource('./api/records');

	// table parameters definition
	$scope.tableParams = new NgTableParams({
		page: 1,            // show first page
		count: 10,          // default count per page
		authorized_person_id: 0,
		region_code: 0,
		subregion_code: 0,
		municipality_code: 0,
        first_name: '',
        last_name: ''
		
//		sorting: {
//			name: 'asc'     // initial sorting
//		}
	}, {
		total: statistics.records,
		paginationMaxBlocks: 9,
		getData: function ($defer, params) {
//			 ajax request to api
			Api.get(params.url(), function (data) { 
				$timeout(function () {
					// update table params
					params.total(data.total);
                    
                    // update statistics
                    statistics.records = data.total;
                    statistics.parcels = data.parcel;
//                    statistics.area = data.area;
					// set new data
					$defer.resolve(data.result);
				}, 500);
			});
		}
	});
	
	$scope.$watch('selectedPerson', function() {
		if($scope.selectedPerson !== null && typeof $scope.selectedPerson !== 'undefined'){
//			console.log('value is', $scope.tableParams.total());
//			$scope.tableParams.$params.authorized_person_id = $scope.selectedPerson.originalObject.authorized_person_id;
            $scope.tableParams.$params.first_name = $scope.selectedPerson.originalObject.first_name;
            $scope.tableParams.$params.last_name = $scope.selectedPerson.originalObject.last_name;
		}
		if($scope.selectedPerson === null){
			$scope.tableParams.$params.authorized_person_id = 0;
            $scope.tableParams.$params.first_name = '';
            $scope.tableParams.$params.last_name = '';
		}
	});
	
	$scope.$watch('selectedRegion', function() {
		if($scope.selectedRegion !== null && typeof $scope.selectedRegion !== 'undefined'){
			$scope.tableParams.$params.region_code = $scope.selectedRegion.code;
		}
	});
	
	$scope.$watch('selectedSubregion', function() {
		if($scope.selectedSubregion !== null && typeof $scope.selectedSubregion === 'object'){
			$scope.tableParams.$params.subregion_code = $scope.selectedSubregion.originalObject.code;
		}
		if($scope.selectedSubregion === null){
			$scope.tableParams.$params.subregion_code = 0;
		}
	});
	
	$scope.$watch('selectedMunicipality', function() {
		if($scope.selectedMunicipality !== null && typeof $scope.selectedMunicipality !== 'undefined'){
			$scope.tableParams.$params.municipality_code = $scope.selectedMunicipality.originalObject.code;
		}
		if($scope.selectedMunicipality === null){
			$scope.tableParams.$params.municipality_code = 0;
		}
	});
});

