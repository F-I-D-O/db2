<?php
//die;
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Mvc\Application;
use Phalcon\DI\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as MysqlAdapter;

require_once '../app/paths.php';
require_once '../app/dbconfig.php';

try {
//	echo 't';
    // Register an autoloader
    $loader = new Loader();
    $loader->registerDirs(array(
        '../app/controllers/',
        '../app/models/',
		'../app/classes/'
    ))->register();

    // Create a DI
    $di = new FactoryDefault();
	
	// Setup the database service
    $di->set('db', function(){
        return new MysqlAdapter(array(
            "host"     => HOST,
			"username" => USER,
			"password" => PASSWORD,
			"dbname"   => DB,
			'charset'   =>'utf8',
			"dialectClass" => "MyDialect"
        ));
    });

    // Setup the view component
    $di->set('view', function(){
        $view = new View();
        $view->setViewsDir('../app/views/');
        return $view;
    });

    // Setup a base URI so that all generated URIs include the "tutorial" folder
    $di->set('url', function(){
        $url = new Url();
        $url->setBaseUri('/db2/');
        return $url;
    });

    // Handle the request
    $application = new Application($di);

    echo $application->handle()->getContent();

} 
catch(\Exception $e) {
    if(Config::DEBUG){
        echo "PhalconException: ", $e->getMessage();
    }
    
}
