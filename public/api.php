<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

//phpinfo();die;

//print_r(get_loaded_extensions()); 

use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\Collection as MicroCollection;
use Phalcon\Db\Adapter\Pdo\Mysql as MysqlAdapter;
use Phalcon\Events\Manager as EventsManager;


require_once '../app/paths.php';
require_once '../app/dbconfig.php';

//require_once '../lib/QueryLogger.php';

$app = new Micro();

/*
 * LOADER
 */

// Register an autoloaderfido 
$loader = new Loader();

$loader->registerDirs(array(
        '../app/controllers/',
        '../app/models/',
		'../app/classes/'
    ))->register();



/*
 * EVENT LISTENER
 */

//Create a events manager
$eventManager = new EventsManager();

//Listen all the application events
$eventManager->attach('micro', function($event, $app) {
    if ($event->getType() == 'beforeExecuteRoute') {
//		echo $app->request->route()->name(); die;
//        if ($app->session->get('auth') == false) {
//
//            $app->flashSession->error("The user isn't authenticated");
//            $app->response->redirect("/")->sendHeaders();
//
//            //Return (false) stop the operation
//            return false;
//        }
    }

});

//Bind the events manager to the app
$app->setEventsManager($eventManager);


if(Config::DEBUG){
	$debug = new \Phalcon\Debug();
	$debug->listen();
}

if(Config::DEBUG_SQL){
//	Phalcon\Mvc\Model::setup(['exceptionOnFailedSave' => true]);
	$di = new \Phalcon\DI\FactoryDefault();
	$application = new \Phalcon\Mvc\Application($di);
	$eventsManager = new \Phalcon\Events\Manager();
//    $queryLogger = new \Phalcon\Db\Profiler\QueryLogger();
	$logger = new \Phalcon\Logger\Adapter\File("../log/db.log");
//	$queryLogger->setLogger($logger);
//    $eventsManager->attach('db', $queryLogger);
	
	$eventsManager->attach('db', function($event, $connection) use ($logger) {
    if ($event->getType() == 'beforeQuery') {
		$logger->log($connection->getSQLStatement(), \Phalcon\Logger::INFO);
		$logger->log($connection->getSQLVariables(), \Phalcon\Logger::INFO);
		print "{$connection->getSQLStatement()}<br>";
		print_r($connection->getSQLVariables()) . '<br>';
    }
});
}

if(!Config::DEBUG_SQL){
	//Setup the database service
	$app['db'] = function() {
		return new MysqlAdapter(array(
			"host"     => HOST,
			"username" => USER,
			"password" => PASSWORD,
			"dbname"   => DB,
			'charset'   =>'utf8',
			"dialectClass" => "MyDialect"
		));
	};
}
else{
	$connection = new MysqlAdapter(array(
		"host" => HOST,
		"username" => USER,
		"password" => PASSWORD,
		"dbname"   => DB,
		'charset'   =>'utf8',
		"dialectClass" => "MyDialect"
	));
	$di->set('db', $connection);
}


if(Config::DEBUG_SQL){
	$connection->setEventsManager($eventsManager);
}

/*
 * NORMAL ACTIONS
 */

//$app->get('/search-lastname/{search}', function ($search) {
//    echo "<h1>Hello! $search</h1>";
//});

$normalActions = new MicroCollection();

$normalActions->setHandler('NormalActionsController', true);

$normalActions->get('/search-lastname', 'searchLastName');

$normalActions->get('/records', 'searchResults');

$normalActions->get('/subregion', 'searchSubregions');

$normalActions->get('/municipality', 'searchMunicipality');

$app->mount($normalActions);



/*
 * SECURE ACTIONS
 */
$secureActions = new MicroCollection();

//Set the main handler. ie. a controller instance
$secureActions->setHandler('SecuredActionsController', true);

//Set a common prefix for all routes
$secureActions->setPrefix('/secure');

$secureActions->get('/download-data-uzsvm', 'downloadDataUZSVM');
$secureActions->get('/convert-data-uzsvm', 'convertDataUZSVM');
$secureActions->get('/import-data-uzsvm', 'importDataFromUZSVMFiles');
$secureActions->get('/import-geography', 'importDataFromGeographyFile');

$app->mount($secureActions);

/*
 * ADMIN ACTIONS
 */
$adminActions = new MicroCollection();

//Set the main handler. ie. a controller instance
$adminActions->setHandler('AdminActionsController', true);

//Set a common prefix for all routes
$adminActions->setPrefix('/admin');

$adminActions->get('/record', 'record');
$adminActions->put('/record/{id}', 'putRecord');
$adminActions->post('/record', 'putRecord');
$adminActions->delete('/record/{id}', 'deleteRecord');

$adminActions->get('/parcel', 'parcel');
$adminActions->put('/parcel/{id}', 'putParcel');
$adminActions->post('/parcel', 'putParcel');
$adminActions->delete('/parcel/{id}', 'deleteParcel');

$adminActions->get('/region', 'region');
$adminActions->put('/region/{id}', 'putRegion');
$adminActions->post('/region', 'putRegion');
$adminActions->delete('/region/{id}', 'deleteRegion');

$adminActions->get('/subregion', 'subregion');
$adminActions->put('/subregion/{id}', 'putSubregion');
$adminActions->post('/subregion', 'putSubregion');
$adminActions->delete('/subregion/{id}', 'deleteSubregion');

$adminActions->get('/municipality', 'municipality');
$adminActions->put('/municipality/{id}', 'putMunicipality');
$adminActions->post('/municipality', 'putMunicipality');
$adminActions->delete('/municipality/{id}', 'deleteMunicipality');

$adminActions->get('/cadastral-area', 'cadastralArea');
$adminActions->put('/cadastral-area/{id}', 'putCadastralArea');
$adminActions->post('/cadastral-area', 'putCadastralArea');
$adminActions->delete('/cadastral-area/{id}', 'deleteCadastralArea');

$adminActions->get('/building', 'building');
$adminActions->put('/building/{id}', 'putBuilding');
$adminActions->post('/building', 'putBuilding');
$adminActions->delete('/building/{id}', 'deleteBuilding');

$adminActions->get('/authorized-person', 'authorizedPerson');
$adminActions->put('/authorized-person/{id}', 'putAuthorizedPerson');
$adminActions->post('/authorized-person', 'putAuthorizedPerson');
$adminActions->delete('/authorized-person/{id}', 'deleteAuthorizedPerson');

$adminActions->get('/natural-person', 'naturalPerson');
$adminActions->put('/natural-person/{id}', 'putNaturalPerson');
$adminActions->post('/natural-person', 'putNaturalPerson');
$adminActions->delete('/natural-person/{id}', 'deleteNaturalPerson');

$adminActions->get('/legal-person', 'legalPerson');
$adminActions->put('/legal-person/{id}', 'putLegalPerson');
$adminActions->post('/legal-person', 'putLegalPerson');
$adminActions->delete('/legal-person/{id}', 'deleteLegalPerson');

$app->mount($adminActions);



$app->notFound(function (){
    echo 'operation not supported';
});



$app->handle();

