<?php

const DIR_SEPARATOR = '/';

const DATA_UZSVM_FOLDER = 'data-uzsvm';

const APP_FOLDER = 'app';

const LIB_FOLDER = 'lib';

const TEMPORARY_FOLDER = 'temp';


define('ROOT', realpath(__DIR__ . "/..//"));

define('APP_ROOT', ROOT . DIR_SEPARATOR . APP_FOLDER);

define('DATA_UZSVM_FOLDER_PATH', ROOT . DIR_SEPARATOR . DATA_UZSVM_FOLDER . DIR_SEPARATOR);

define('PCLZIP_LIBRARY_PATH', ROOT . DIR_SEPARATOR . LIB_FOLDER . DIR_SEPARATOR . 'xlsx2csv/pclzip/pclzip.lib.php');

define('TEMPORARY_FOLDER_PATH', ROOT . DIR_SEPARATOR . TEMPORARY_FOLDER);


//echo DATA_UZSVM_FOLDER_PATH;

