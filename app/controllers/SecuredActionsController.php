<?php

define('UZSVM_DOWNLOAD_LINKS', serialize(array('Praha' => '1059210', 'Benesov' => '1056955')));

define('UZSVM_DOWNLOAD_GLOBAL_LINK', '1059287');

/**
 * Description of SecuredActionController
 *
 * @author david_000
 */
class SecuredActionsController extends LoaderController
{
	
	public function onConstruct()
    {
		if(!$this->isUserAuthorized()){
			echo 'ACCESS DENIDED';
			exit;
		}
		set_time_limit(100000);
		ini_set('memory_limit','1024M');
//		phpinfo();
//		die;
    }
	
	public function downloadDataUZSVM(){
		$this->deleteFilesFromUZSVMDataFolder();
		$link = "http://uzsvm.cz/Soubor.ashx?docsouborID=" . UZSVM_DOWNLOAD_GLOBAL_LINK;
		echo "Downloading file from: {$link}\n\n";
		file_put_contents("../data-uzsvm/" . ImportUZSVM::FILENAME, fopen("{$link}", 'r'));
	}
    
    public function downloadDataUZSVMallLinks(){
		foreach (unserialize(UZSVM_DOWNLOAD_LINKS) as $area => $code) {
			echo "Downloading file from: http://uzsvm.cz/Soubor.ashx?docsouborID={$code}\n\n";
			file_put_contents("../data-uzsvm/{$area}.xlsx", fopen("http://uzsvm.cz/Soubor.ashx?docsouborID={$code}", 'r'));
		}
	}
	
	public function convertDataUZSVM(){
		$convertor = new XlsxToCsv(ImportUZSVM::FILENAME, DATA_UZSVM_FOLDER_PATH);
		$convertor->convert();
	}
    
	
	public function importDataFromUZSVMFiles(){
		$startIndex = $this->request->hasQuery("start_index") ? $this->request->getQuery("start_index", 'int') : 1;
		$import = new ImportUZSVM($startIndex);
		$import->runWithLog();
	}
	
	public function importDataFromGeographyFile(){
		$import = new ImportGeography();
		$import->runWithLog();
	}
	
	private function deleteFilesFromUZSVMDataFolder(){
		$files = glob(DATA_UZSVM_FOLDER_PATH . '*'); // get all file names
		foreach($files as $file){ // iterate files
			if(is_file($file)){
				unlink($file); // delete file
			}
		}
	}
	private function isUserAuthorized(){
//		var_dump($this->request->getQuery("key", 'int'));
		return $this->request->hasQuery("key") && $this->request->getQuery("key", 'int') == 5687945632154785813;
	}
}
