<?php

use Phalcon\Mvc\Controller;
use \Phalcon\Db\Column;

/**
 * Description of NormalActionsController
 *
 * @author Fido
 */
class NormalActionsController extends Controller {
    
    private $parametrToModel = [
        'naturl_person_lastname' => 'NaturalPerson.last_name',
        'municipality_name' => 'Municipality.name',
        'area' => 'Parcel.area',
        
    ];
	
	public function searchMunicipality() {
		
		$where = [];
		$params = [];
		$sql = "SELECT Municipality.name, Municipality.code FROM Municipality 
					INNER JOIN Subregion ON Municipality.subregion_code = Subregion.code
					INNER JOIN Region ON Subregion.region_code = Region.code
				";
		
		if(!empty($this->request->getQuery("name"))){
			$where [] = "USE_ACCENTS_LOWER(Municipality.name) LIKE :name: OR USE_ACCENTS(Municipality.name) LIKE :name: ";
			$params['name'] = "{$this->request->getQuery("name")}%";
		}
		if(!empty($this->request->getQuery("region", 'int'))){
			$where [] = "region_code = :regionCode: ";
			$params['regionCode'] = $this->request->getQuery("region", 'int');
		}
		if(!empty($this->request->getQuery("subregion", 'int'))){
			$where [] = "subregion_code = :subregionCode: ";
			$params['subregionCode'] = $this->request->getQuery("subregion", 'int');
		}
		
		if(!empty($where)){
			$sql .= "WHERE " . implode(" AND ", $where);
		}
		
		$regions = $this->modelsManager->executeQuery($sql, $params);
		
		echo json_encode($regions->toArray(), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function searchSubregions() {
		$query = Subregion::query()->columns('name, code');			
		
		$bindParams = [];
		if(!empty($this->request->getQuery("name"))){
			$query->andWhere("USE_ACCENTS_LOWER(name) LIKE :name: ")
				->orWhere("USE_ACCENTS(name) LIKE :name: ");
			$bindParams['name'] = "{$this->request->getQuery("name")}%";
		}
		if(!empty($this->request->getQuery("region", 'int'))){
			$query->andWhere("region_code = :regionCode: ");
			$bindParams['regionCode'] = $this->request->getQuery("region", 'int');
		}
		
		$regions = $query->bind($bindParams)->execute();
		
		echo json_encode($regions->toArray(), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function searchLastName() {
		
//		$naturalPersons = NaturalPerson::find(array("last_name LIKE :lastName:%", 
//			'bind' => array('lastName' => $searchString),
//			'columns' => 'first_name, last_name'));
		$searchString = $this->request->getQuery("name");
        
        $params = [
            'conditions' => 'USE_ACCENTS_LOWER(last_name) LIKE :lastName: OR USE_ACCENTS(last_name) LIKE :lastName:',
            'bind' => ['lastName' => "{$searchString}%"],
            'columns' => 'first_name, last_name',
            'group' => 'last_name, first_name',
            'order' => 'USE_ACCENTS_LOWER(last_name)'
        ];
        $naturalPersons = NaturalPerson::find($params);   
        
//		$naturalPersons = NaturalPerson::query()
//				->where("USE_ACCENTS_LOWER(last_name) LIKE :lastName: ")
//				->orWhere("USE_ACCENTS(last_name) LIKE :lastName: ")
//				->bind(array('lastName' => "{$searchString}%"))
//				->columns('first_name, last_name, authorized_person_id')
//                ->group('last_name')        
//                ->orderBy('USE_ACCENTS_LOWER(last_name)')
//				->execute();
		
		echo json_encode($naturalPersons->toArray(), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function searchResults() {
		$params = [];
		
		$columnsSelectString = "SELECT AuthorizedPerson.id AS authorized_person_id, 
					Parcel.number AS parcel_number, 
					Parcel.area, 
					Parcel.way_of_using, 
					Municipality.name AS municipality_name,
                    CONCAT(NaturalPerson.first_name, ' ', NaturalPerson.last_name) AS owner,
                    CONCAT(Record.ownership_share_numerator, '/', Record.ownership_share_denominator) AS share,
                    NaturalPerson.adress,
                    Parcel.type AS parcel_type,
                    Parcel.lv_number,
                    Region.name AS region,
                    Subregion.name AS subregion";
		$countSelectString = "SELECT COUNT (*) AS count_total, COUNT(DISTINCT Parcel.id) AS parcel_total, 
                    SUM(Parcel.area) AS area_total";
		$sql = $this->getSearchSql($params);
		
        $dataTotal = $this->modelsManager->executeQuery($countSelectString . $sql, $params)->getFirst();
		$count = $dataTotal['count_total'];
//		var_dump($count);die;
	
		// phalcon can't bind limit :(
		$limitTo = $this->request->getQuery("count", 'int');
		$limitFrom = ($this->request->getQuery("page", 'int') - 1) * $limitTo;
		$sql .= " LIMIT {$limitFrom}, {$limitTo}";
		
		$rows  = $this->modelsManager->executeQuery($columnsSelectString . $sql, $params);

		$response = new RecordSearchResult($count, $dataTotal['parcel_total'], $dataTotal['area_total'], $rows->toArray());	
		
		echo json_encode($response, JSON_NUMERIC_CHECK);
		exit;
	}
	
	private function getSearchSql(&$params){
		$where = [];
//		$types = [];
		
		$sql = " FROM AuthorizedPerson 
					INNER JOIN Record ON AuthorizedPerson.id = Record.authorized_person_id 
					INNER JOIN Parcel ON Record.parcel_id = Parcel.id 
					INNER JOIN CadastralArea ON Parcel.cadastral_area_code = CadastralArea.code
					INNER JOIN Municipality ON CadastralArea.municipality_code = Municipality.code
					INNER JOIN Subregion ON Municipality.subregion_code = Subregion.code
					INNER JOIN Region ON Subregion.region_code = Region.code
                    INNER JOIN NaturalPerson ON AuthorizedPerson.id = NaturalPerson.authorized_person_id
				";
		
		if(!empty($this->request->getQuery("authorized_person_id", 'int'))){
			$where[] = "AuthorizedPerson.id = :authorizedPersonId:";
			$params['authorizedPersonId'] = $this->request->getQuery("authorized_person_id", 'int');
		}
        
        if(!empty($this->request->getQuery("first_name"))){
			$where[] = "USE_ACCENTS(NaturalPerson.first_name) = :firstName:";
			$params['firstName'] = urldecode($this->request->getQuery("first_name"));
		}
        
        if(!empty($this->request->getQuery("last_name"))){
			$where[] = "USE_ACCENTS(NaturalPerson.last_name) = :lastName:";
			$params['lastName'] = urldecode($this->request->getQuery("last_name"));
		}
		
		if(!empty($this->request->getQuery("region_code", 'int'))){
			$where[] = "Region.code = :regionCode:";
			$params['regionCode'] = $this->request->getQuery("region_code", 'int');
		}
		
		if(!empty($this->request->getQuery("subregion_code", 'int'))){
			$where[] = "Subregion.code = :subregionCode:";
			$params['subregionCode'] = $this->request->getQuery("subregion_code", 'int');
		}
		
		if(!empty($this->request->getQuery("municipality_code", 'int'))){
			$where[] = "Municipality.code = :municipalityCode:";
			$params['municipalityCode'] = $this->request->getQuery("municipality_code", 'int');
		}
		
		if(!empty($where)){
			$sql .= "WHERE " . implode(" AND ", $where);
		}
        
        if(!empty($this->request->getQuery("sorting"))){
            $sorting = $this->request->getQuery("sorting");
            $sql .= "ORDER BY ";
            $resolvedParametrs = [];
            foreach ($sorting as $parameter => $direction) {
                $parameter = $this->parametrToModel[$parameter];
                $resolvedParametrs[] = "{$parameter} {$direction}";
            }
            $sql .= implode(', ', $resolvedParametrs);
        }
        
		return $sql;
	}
    
}
