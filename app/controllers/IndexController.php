<?php

use Phalcon\Mvc\Controller;

class IndexController extends Controller
{

    public function indexAction()
    {
		$regionOptions = Region::query()
				->columns('code, name')
				->execute()
				->toArray();
		$regionOptions[0] = [code => 0, name => 'Zvolte kraj'];
//		var_dump($regionOptions);die;
		$this->view->regions = htmlspecialchars(json_encode($regionOptions, JSON_NUMERIC_CHECK));
    }
	
	

}