<?php

use Phalcon\Mvc\Controller;

class AdminActionsController extends Controller {
	
	public function record() {
		$count = $this->request->getQuery("count", 'int');
		$startRecord = ($this->request->getQuery("page", 'int') - 1) * $count;
		$records = Record::query()
				->limit($count, $startRecord)
				->execute();
		
		echo json_encode(new TableResult(Record::count(), $records->toArray()), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function putRecord($id = NULL) {
		$recordData = $this->request->getJsonRawBody(true);
//		var_dump($record);
		$record = new Record();
		if(!$record->save($recordData)){
			foreach ($record->getMessages() as $message) {
				echo $message, "\n";
			}
		}
		
	}
	
	public function deleteRecord($id) {
		$record = Record::find($id);
		$record->delete();
	}
	
	
	public function parcel() {
		$count = $this->request->getQuery("count", 'int');
		$startRecord = ($this->request->getQuery("page", 'int') - 1) * $count;
		$records = Parcel::query()
				->limit($count, $startRecord)
				->execute();
		
		echo json_encode(new TableResult(Parcel::count(), $records->toArray()), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function putParcel($id = NULL) {
		$recordData = $this->request->getJsonRawBody(true);
//		var_dump($record);
		$record = new Parcel();
		if(!$record->save($recordData)){
			foreach ($record->getMessages() as $message) {
				echo $message, "\n";
			}
		}
		
	}
	
	public function deleteParcel($id) {
		$record = Parcel::find($id);
		$record->delete();
	}
	
	
	public function region() {
		$count = $this->request->getQuery("count", 'int');
		$startRecord = ($this->request->getQuery("page", 'int') - 1) * $count;
		$records = Region::query()
				->limit($count, $startRecord)
				->execute();
		
		echo json_encode(new TableResult(Region::count(), $records->toArray()), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function putRegion($id = NULL) {
		$recordData = $this->request->getJsonRawBody(true);
//		var_dump($record);
		$record = new Region();
		if(!$record->save($recordData)){
			foreach ($record->getMessages() as $message) {
				echo $message, "\n";
			}
		}
		
	}
	
	public function deleteRegion($id) {
		$record = Region::find($id);
		$record->delete();
	}
	
	
	public function subregion() {
		$count = $this->request->getQuery("count", 'int');
		$startRecord = ($this->request->getQuery("page", 'int') - 1) * $count;
		$records = Subregion::query()
				->limit($count, $startRecord)
				->execute();
		
		echo json_encode(new TableResult(Subregion::count(), $records->toArray()), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function putSubregion($id = NULL) {
		$recordData = $this->request->getJsonRawBody(true);
//		var_dump($record);
		$record = new Subregion();
		if(!$record->save($recordData)){
			foreach ($record->getMessages() as $message) {
				echo $message, "\n";
			}
		}
		
	}
	
	public function deleteSubregion($id) {
		$record = Subregion::find($id);
		$record->delete();
	}
	
	
	public function municipality() {
		$count = $this->request->getQuery("count", 'int');
		$startRecord = ($this->request->getQuery("page", 'int') - 1) * $count;
		$records = Municipality::query()
				->limit($count, $startRecord)
				->execute();
		
		echo json_encode(new TableResult(Municipality::count(), $records->toArray()), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function putMunicipality($id = NULL) {
		$recordData = $this->request->getJsonRawBody(true);
//		var_dump($record);
		$record = new Municipality();
		if(!$record->save($recordData)){
			foreach ($record->getMessages() as $message) {
				echo $message, "\n";
			}
		}
		
	}
	
	public function deleteMunicipality($id) {
		$record = Municipality::find($id);
		$record->delete();
	}
	
	
	
	public function cadastralArea() {
		$count = $this->request->getQuery("count", 'int');
		$startRecord = ($this->request->getQuery("page", 'int') - 1) * $count;
		$records = CadastralArea::query()
				->limit($count, $startRecord)
				->execute();
		
		echo json_encode(new TableResult(CadastralArea::count(), $records->toArray()), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function putCadastralArea($id = NULL) {
		$recordData = $this->request->getJsonRawBody(true);
//		var_dump($record);
		$record = new CadastralArea();
		$recordData['name_unique'] = $recordData['name'];
		if(!$record->save($recordData)){
			foreach ($record->getMessages() as $message) {
				echo $message, "\n";
			}
		}
		
	}
	
	public function deleteCadastralArea($id) {
		$record = Building::find($id);
		$record->delete();
	}
	
	
	public function building() {
		$count = $this->request->getQuery("count", 'int');
		$startRecord = ($this->request->getQuery("page", 'int') - 1) * $count;
		$records = Building::query()
				->limit($count, $startRecord)
				->execute();
		
		echo json_encode(new TableResult(Building::count(), $records->toArray()), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function putBuilding($id = NULL) {
		$recordData = $this->request->getJsonRawBody(true);
//		var_dump($record);
		$record = new Building();
		if(!$record->save($recordData)){
			foreach ($record->getMessages() as $message) {
				echo $message, "\n";
			}
		}
		
	}
	
	public function deleteBuilding($id) {
		$record = Building::find($id);
		$record->delete();
	}
	
	
	public function authorizedPerson() {
		$count = $this->request->getQuery("count", 'int');
		$startRecord = ($this->request->getQuery("page", 'int') - 1) * $count;
		$records = AuthorizedPerson::query()
				->limit($count, $startRecord)
				->execute();
		
		echo json_encode(new TableResult(AuthorizedPerson::count(), $records->toArray()), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function putAuthorizedPerson($id = NULL) {
		$recordData = $this->request->getJsonRawBody(true);
		$record = new AuthorizedPerson();
		if(!$record->save($recordData)){
			foreach ($record->getMessages() as $message) {
				echo $message, "\n";
			}
		}
		
	}
	
	public function deleteAuthorizedPerson($id) {
		$record = AuthorizedPerson::find($id);
		$record->delete();
	}
	
	
	public function naturalPerson() {
		$count = $this->request->getQuery("count", 'int');
		$startRecord = ($this->request->getQuery("page", 'int') - 1) * $count;
		$records = NaturalPerson::query()
				->limit($count, $startRecord)
				->execute();
		
		echo json_encode(new TableResult(NaturalPerson::count(), $records->toArray()), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function putNaturalPerson($id = NULL) {
		$recordData = $this->request->getJsonRawBody(true);
		$record = new NaturalPerson();
		if(!$record->save($recordData)){
			foreach ($record->getMessages() as $message) {
				echo $message, "\n";
			}
		}
		
	}
	
	public function deleteNaturalPerson($id) {
		$record = NaturalPerson::find($id);
		$record->delete();
	}
	
	
	public function legalPerson() {
		$count = $this->request->getQuery("count", 'int');
		$startRecord = ($this->request->getQuery("page", 'int') - 1) * $count;
		$records = LegalPerson::query()
				->limit($count, $startRecord)
				->execute();
		
		echo json_encode(new TableResult(LegalPerson::count(), $records->toArray()), JSON_NUMERIC_CHECK);
		exit;
	}
	
	public function putLegalPerson($id = NULL) {
		$recordData = $this->request->getJsonRawBody(true);
		$record = new LegalPerson();
		if(!$record->save($recordData)){
			foreach ($record->getMessages() as $message) {
				echo $message, "\n";
			}
		}
		
	}
	
	public function deleteputLegalPerson($id) {
		$record = LegalPerson::find($id);
		$record->delete();
	}
}
