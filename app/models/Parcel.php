<?php

use Phalcon\Mvc\Model;

/**
 * Description of Region
 *
 * @author Fido
 */
class Parcel extends Model {
	
	public function initialize()
    {
        $this->hasMany('id', 'Record', 'parcel_id');
		$this->belongsTo('cadastral_area_code', 'CadastralArea', 'code');
    }
}
