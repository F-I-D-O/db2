<?php

use Phalcon\Mvc\Model;

/**
 * Description of Region
 *
 * @author Fido
 */
class Municipality extends Model {
	
	public function initialize()
    {
        $this->hasMany('code', 'CadastralArea', 'municipality_code');
		$this->belongsTo('subregion_code', 'Subregion', 'code');
    }
}
