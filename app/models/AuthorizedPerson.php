<?php

use Phalcon\Mvc\Model;

/**
 * Description of Region
 *
 * @author Fido
 */
class AuthorizedPerson extends Model {
	
	public function initialize()
    {
        $this->hasMany('id', 'Record', 'authorized_person_id');
    }
}
