<?php

use Phalcon\Mvc\Model;

/**
 * Description of Region
 *
 * @author Fido
 */
class Subregion extends Model {
	
	public function initialize()
    {
        $this->hasMany('code', 'Municipality', 'subregion_code');
		$this->belongsTo('region_code', 'Region', 'code');
    }
	
}
