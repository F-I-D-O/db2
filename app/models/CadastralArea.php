<?php

use Phalcon\Mvc\Model;

/**
 * Description of Region
 *
 * @author Fido
 */
class CadastralArea extends Model {
	
	public function initialize()
    {
        $this->hasMany('code', 'Parcel', 'cadastral_area_code');
		$this->belongsTo('municipality_code', 'Municipality', 'code');
    }
}
