<?php

use Phalcon\Mvc\Model;

/**
 * Description of Region
 *
 * @author Fido
 */
class Record extends Model {
	
	public function initialize()
    {
        $this->belongsTo('parcel_id', 'Parcel', 'id');
		$this->belongsTo('authorized_person_id', 'AuthorizedPerson', 'id');
    }
}
