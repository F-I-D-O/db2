<?php

use Phalcon\Mvc\Model;

/**
 * Description of Region
 *
 * @author Fido
 */
class Region extends Model {
	
	
	public function initialize()
    {
        $this->hasMany('code', 'Subregion', 'region_code');
    }
}
