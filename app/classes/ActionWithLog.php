<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActionWithLog
 *
 * @author Fido
 */
abstract class ActionWithLog {
	
	const DEFAULT_ENCODING = 'utf-8';
	
	private $encoding;
	
	
	protected function __construct($logEncoding = self::DEFAULT_ENCODING){
		$this->encoding = $logEncoding;
	}

	public function runWithLog() {
		$this->start();
		$this->run();
		$this->end();
	}
	
	public abstract function run();
	
	private function start(){
		print "<!DOCTYPE html> 
				<html lang='cs'>
					<head>
						<meta charset='{$this->encoding}'>

						<title>Log</title>

					</head>
					<body>";
	}
	
	private function end(){
		print		"</body>
				</html>";
	}
}
