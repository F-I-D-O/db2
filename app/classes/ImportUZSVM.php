<?php

require_once '../lib/PHPExcel/Classes/PHPExcel.php';

require('../lib/spreadsheet-reader/SpreadsheetReader.php');



/**
 * Description of ImportUZSVM
 *
 * @author Fido
 */
class ImportUZSVM extends ActionWithLog {
	
	const CHUNK_SIZE = 1000;
	
	const FILENAME = 'cela_cr.xlsx';
	
	
	private $startIndex;
	
	
	public function __construct($startIndex = 1) {
		parent::__construct();
		$this->startIndex = $startIndex;
	}

	
	public function run(){
		$files = scandir(DATA_UZSVM_FOLDER_PATH);
		foreach($files as $filename) {
//			if(String::endsWith($filename, ".xlsx")){
//				$this->importDataFromUZSVMFile($filename);
//			}
			if(String::endsWith($filename, ".csv")){
				$this->importDataFromUZSVMFile($filename);
			}
		}
	}
	
	public function importDataFromUZSVMFile($filename){
		$Reader = new SpreadsheetReader(DATA_UZSVM_FOLDER_PATH . $filename);
		
		$rowIndex = 0;
		
//		var_dump(is_callable('Debug::exceptionErrorHandler'));die;
		
		set_error_handler('Debug::exceptionErrorHandler');
		
		foreach ($Reader as $rowData)
		{
			$rowIndex++;
//			print_r($rowData);
//			die;
			
			//skip firs row and rows before start index
			if($rowIndex === 1 || $rowIndex < $this->startIndex){
				continue;
			}
			try {
				$this->insertRecordInDb($rowData);
			}
			catch (ErrorException $e){
				echo "Error exception on row: {$rowIndex}<br>";
				echo "Message: " . $e->getMessage() . " in: " . $e->getFile() . " on line: " . $e->getLine() . "<br>";
				
				echo "Row data: ";
				print_r($rowData);
				exit;
			}
			catch (UnsuccessfulSaveException $e){
				echo "Save exception on row: {$rowIndex}<br>";
				$e->printException();
				echo "Row data: ";
				print_r($rowData);
				exit;
			}
		}
		
		restore_error_handler();
	}
	
	public function importDataFromUZSVMFilePHPExcelCSV($filename){
		$objReader = new PHPExcel_Reader_CSV();
		$objReader->setDelimiter(';');
		$objReader->setInputEncoding('utf-8');
		
		$objPHPExcel = $objReader->load(DATA_UZSVM_FOLDER_PATH . $filename);

		$sheet = $objPHPExcel->getSheet(0); 
		
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();
		
		for ($row = 2; $row <= $highestRow; $row++){ 
			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
			$this->insertRecordInDb($rowData[0]);
		}
	}
	
	public function importDataFromUZSVMFilePHPExcel($filename){
		echo "Reading file: {$filename} \n";
		
		
		$objReader = new PHPExcel_Reader_Excel2007();
		
		$objReader->setReadDataOnly(true);
		
		$chunkFilter = new PHPExcelChunkReadFilter();
		
		/**  Tell the Reader that we want to use the Read Filter that we've Instantiated  **/
		$objReader->setReadFilter($chunkFilter);
		
		for ($startRow = 2; ; $startRow += self::CHUNK_SIZE) { 

			/**  Tell the Read Filter, the limits on which rows we want to read this iteration  **/ 
			$chunkFilter->setRows($startRow, self::CHUNK_SIZE); 

			/**  Load only the rows that match our filter from $inputFileName to a PHPExcel Object  **/ 
			$objPHPExcel = $objReader->load(DATA_UZSVM_FOLDER_PATH . $filename); 
			
			if($objPHPExcel->getSheetCount() == 0){
				echo "zero sheet count - end \n";
				return;
			}

			$sheet = $objPHPExcel->getSheet(0); 
//			var_dump($sheet);die;
		
			$highestRow = $sheet->getHighestRow(); 
			$highestColumn = $sheet->getHighestColumn();
			
			if($sheet->getHighestRow() == 1){
				echo "zero row count - end \n";
				return;
			}
			
//			echo $highestRow;

			for ($row = $startRow; $row <= $highestRow; $row++){ 
				//  Read a row of data into an array
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
				$sav = $rowData;
				$rowData = $rowData[0];
				if(empty($rowData[8])){
					print_r($sav);
					echo "empty row: {$row} - end \n";
					return;
				}
				$this->insertRecordInDb($rowData);
//				echo "Row number> {$row} loaded. \n";
			}

			$objPHPExcel->disconnectWorksheets(); 
			unset($objPHPExcel);
			unset($sheet);
			echo "Rows from {$startRow} to " . ($startRow + self::CHUNK_SIZE) . " loaded.";
		}
		
	}
	
	private function insertRecordInDb($rowData){
		$parcelData = $this->getParcelData($rowData);

		$parcel = Parcel::findFirst(["number = :parcelNumber: AND cadastral_area_code = :cadastralAreaCode:", 
			'bind' => ['parcelNumber' => $parcelData['number'], 
				'cadastralAreaCode' => $parcelData['cadastral_area_code']]]);

		if(!$parcel){
			$parcel = new Parcel();
			$suc = $parcel->save($parcelData);
//			var_dump($suc);
//			var_dump($parcel->getMessages());
			if(!empty($parcel->getMessages())){
				throw new UnsuccessfulSaveException($parcel->getMessages());
			}
		}
		
		// building
		if(array_key_exists(19, $rowData) && !empty($rowData[19])){
			$building = Building::findFirst(array("parcel_id = :parcelId:", 'bind' => ['parcelId' => $parcel->id]));
			if(!$building){
				$building = new Building();
				$buildingData = $this->getBuildingData($rowData, $parcel->id);
				$building->save($buildingData);
				if(!empty($building->getMessages())){
					throw new UnsuccessfulSaveException($building->getMessages());
				}
			}
		}
		
		$authorizedPerson = AuthorizedPerson::findFirst(array("id = :authorizedPersonId:", 'bind' => 
				array('authorizedPersonId' => $rowData[8])));
		
		if(!$authorizedPerson){
			$authorizedPerson = new AuthorizedPerson();
			$authorizedPerson->save(['id' => $rowData[8], 'type' => $rowData[4]]);
			if(!empty($authorizedPerson->getMessages())){
				throw new UnsuccessfulSaveException($authorizedPerson->getMessages());
			}
		}
		
		// record for natural person
		if($this->isNaturalPerson($rowData)){
			$naturalPerson = NaturalPerson::findFirst(array("authorized_person_id = :authorizedPersonId:", 'bind' => 
				array('authorizedPersonId' => $authorizedPerson->id)));
			if(!$naturalPerson){
				$naturalPerson = new NaturalPerson();
				$nameArray = explode(' ', $rowData[6]);
				
				// only one word in name - we consider it as last name
				if(count($nameArray) === 1){
					$naturalPerson->save(['authorized_person_id' => $authorizedPerson->id, 'last_name' => $nameArray[0],
						'adress' => $rowData[7]]);
				}
				else {
					$naturalPerson->save(['authorized_person_id' => $authorizedPerson->id, 'first_name' => $nameArray[1],
					'last_name' => $nameArray[0], 'adress' => $rowData[7]]);
				}
				if(!empty($naturalPerson->getMessages())){
					throw new UnsuccessfulSaveException($naturalPerson->getMessages());
				}
			}
		}
		
		// record for legal person
		else{
			$legalPerson = LegalPerson::findFirst(array("authorized_person_id = :authorizedPersonId:", 'bind' => 
				array('authorizedPersonId' => $authorizedPerson->id)));
			if(!$legalPerson){
				$legalPerson = new LegalPerson();
				$legalPerson->save(['authorized_person_id' => $authorizedPerson->id, 'name' => $rowData[6],
					'adress' => $rowData[7]]);
				if(!empty($legalPerson->getMessages())){
					throw new UnsuccessfulSaveException($legalPerson->getMessages());
				}
			}
		}
		
		$record = new Record();
//		var_dump($parcel);
		$record->save(['authorized_person_id' => $authorizedPerson->id, 'parcel_id' => $parcel->id,
			'ownership_share_numerator' => $rowData[9], 'ownership_share_denominator' => $rowData[10], 
			'legal_relation' => $rowData[11]]);
		if(!empty($record->getMessages())){
			throw new UnsuccessfulSaveException($record->getMessages());
		}
	}
	
	private function getParcelData($rowData) {
		$parcelString = $rowData[15];
		$parcelArray = explode(', ', $parcelString);
		$cadastralAreaCode = preg_replace('~[^0-9].*([0-9]{6})~', '\\1', $parcelArray[0]);
//		$parcelNumber = preg_replace('~č\. ([0-9/]+)~', '\\1', $parcelArray[1]);
//		print_r($rowData);
		$parcelNumber = $parcelArray[1];
		return array('cadastral_area_code' => $cadastralAreaCode, 'number' => $parcelNumber, 'area' => $rowData[12], 
			'type' => $rowData[13], 'way_of_using' => $rowData[14], 'lv_number' => $rowData[16]);
	}
	
	private function getBuildingData($rowData, $parcelId) {
//		print_r($rowData);
		$buildingString = $rowData[19];
		$buildingArray = explode(',', $buildingString);
		$houseNumber = preg_replace('~[^0-9].*([0-9]+)~', '\\1', $buildingArray[0]);
		return ['parcel_id' => $parcelId, 'municipality_part' => $rowData[17], 'house_number' => $houseNumber, 
			'way_of_using' => $rowData[18]];
	}
	
	private function isNaturalPerson($rowData){
		return $rowData[4] === 'OFO';
	}
}

