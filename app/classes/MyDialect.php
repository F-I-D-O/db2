<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MyDialect
 *
 * @author Fido
 */
class MyDialect extends \Phalcon\Db\Dialect\MySQL
{

    /**
     * Transforms an intermediate representation for a expression into a database system valid expression. For upload 
     * to hosting it's necessary to remove array from function declaratrion.
     *
     * @param array expression
     * @param string escapeChar
     * @return string
     */
    public function getSqlExpression(array $expression, $escapeChar = null, $bindCounts = NULL)
    {
        if ($expression["type"] == 'functionCall') {
            if ($expression["name"] == 'USE_ACCENTS_LOWER') {
//				print_r($expression["arguments"][0]);die;
				$domain = empty($expression["arguments"][0]['domain']) ? "" : "{$expression["arguments"][0]['domain']}.";
				return "LOWER({$domain}{$expression["arguments"][0]['name']}) COLLATE utf8_bin";
            }
			else if ($expression["name"] == 'USE_ACCENTS') {
//				print_r($expression);die;
				$domain = empty($expression["arguments"][0]['domain']) ? "" : "{$expression["arguments"][0]['domain']}.";
				return "{$domain}{$expression["arguments"][0]['name']} COLLATE utf8_bin";
            }
        }
		
		

        return parent::getSqlExpression($expression, $escapeChar);
    }

}
