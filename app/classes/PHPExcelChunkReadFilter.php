<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PHPExcelChunkReadFilter
 *
 * @author Fido
 */
class PHPExcelChunkReadFilter implements PHPExcel_Reader_IReadFilter {
	
	const DEFAULT_CHUNK_SIZE = 1000;
	
	private $startRow = 0;

    private $endRow = 0;

    /**  Set the list of rows that we want to read  */
    public function setRows($startRow, $chunkSize = self::DEFAULT_CHUNK_SIZE) {
        $this->startRow = $startRow;
        $this->endRow = $startRow + $chunkSize;
		print "Start row set to: {$this->startRow}, end row set to: {$this->endRow} \n";
    }

	
	public function readCell($column, $row, $worksheetName = '') {
		 //  Only read the heading row, and the rows that are configured in $this->_startRow and $this->_endRow
        if ($row >= $this->startRow && $row < $this->endRow && $column <= 20) {
//			print "Row: {$row} accepted \n";
            return true;
        }
        return false;
	}
}
