<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RecordSearchResult
 *
 * @author david_000
 */
class RecordSearchResult extends TableResult {
    public $area;
	public $parcel;
    
    public function __construct($total, $parcel, $area, $data) {
        parent::__construct($total, $data);
        $this->area = $area;
        $this->parcel = $parcel;
    }

}
