<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UnsuccessfulSaveExceprion
 *
 * @author Fido
 */
class UnsuccessfulSaveException extends Exception {
	
	const MESSAGE = "Unsuccessfull save query";
	
	private $queryMessages;


	// Redefine the exception so message isn't optional
    public function __construct($queryMessages) {
        parent::__construct(self::MESSAGE, 0, null);
		$this->queryMessages = $queryMessages;
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function printQueryMessage() {
		foreach ($this->queryMessages as $message) {
			var_dump($message);
		}
    }
	
	public function printException(){
		print "Model->save was not succesful because:<br>";
		foreach ($this->queryMessages as $message) {
			var_dump($message);
		}
	}
}
