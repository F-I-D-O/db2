<?php

require_once '../lib/PHPExcel/Classes/PHPExcel.php';

/**
 * Description of ImportUZSVM
 *
 * @author Fido
 */
class ImportGeography extends ActionWithLog {
	
	private $objPHPExcel;
	
	const FILENAME = 'SC_SEZNAMKUKRA_DOTAZ.csv';
	
	
	
	
	public function __construct() {
		parent::__construct();
	}

	
	public function run(){
		$objReader = new PHPExcel_Reader_CSV();
		$objReader->setDelimiter(';');
		$objReader->setInputEncoding('utf-8');
		
//		var_dump(realpath(ROOT . DIR_SEPARATOR));
//		print_r(scandir(ROOT . DIR_SEPARATOR));
//		
//		var_dump(realpath(ROOT . DIR_SEPARATOR . self::FILENAME));
//		die;
		
		$objPHPExcel = $objReader->load(ROOT . DIR_SEPARATOR . self::FILENAME);

		$sheet = $objPHPExcel->getSheet(0); 
		
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();
		
		for ($row = 2; $row <= $highestRow; $row++){ 
			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
			$this->insertRecordInDb($rowData[0]);
		}
	}
	
	private function insertRecordInDb($rowData){
//		print_r($rowData);
//		die;
		
		$municipality = Municipality::findFirst(array("code = :municipalityCode:", 
					'bind' => array('municipalityCode' => $rowData[5])));
		if(!$municipality){
			$subregion = Subregion::findFirst(array("code = :subregionCode:", 
				'bind' => array('subregionCode' => $rowData[2])));
			if(!$subregion){
				$region = Region::findFirst(array("code = :regionCode:", 'bind' => array('regionCode' => $rowData[0])));
				if(!$region){
					$region = new Region();
					$region->create(['code' => $rowData[0], 'name' => $rowData[1]]);
				}
				$subregion = new Subregion();
				$subregion->create(['code' => $rowData[2], 'region_code' => $rowData[0], 'name' => $rowData[4]]);
			}
			$municipality = new Municipality();
			$municipality->create(['code' => $rowData[5], 'subregion_code' => $rowData[2], 'name' => $rowData[6]]);
		}
		$cadastralArea = new CadastralArea();
		$cadastralArea->create(['code' => $rowData[7], 'municipality_code' => $rowData[5], 'name' => $rowData[9], 
				'name_unique' => $rowData[9]]);
//		var_dump($cadastralArea->getMessages());
//		foreach ($cadastralArea->getMessages() as $message) {
//			var_dump($message);
//		}
//		die;
	}
}

